export default function checkAuth({ store, redirect }) {
  if (!store.state.auth.user) {
    redirect('/login');
  }
}
