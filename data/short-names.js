export const shortNames = {
  en: 'English',
  cn: 'Chinese',
  fr: 'French',
  sp: 'Spanish',
  pt: 'Portuguese',
  ru: 'Russian',
  ua: 'Ukrainian',
  jp: 'Japanese',
  ko: 'Korean',
};
