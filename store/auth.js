import firebase from 'firebase';

const defUser = localStorage.getItem('token') || null;

export const state = () => ({
  user: defUser,
});

export const mutations = {
  setUser(state, payload) {
    state.user = payload;
  },
};

export const actions = {
  async signUpAction({ commit }, payload) {
    try {
      const response = await firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password);
      localStorage.setItem('token', response.user.uid);
      commit('setUser', response.user.uid);
    } catch (error) {
      this.$toast.error(error.message);
    }
  },
  async signInAction({ commit }, payload) {
    try {
      const response = await firebase.auth().signInWithEmailAndPassword(payload.email, payload.password);
      localStorage.setItem('token', response.user.uid);
      commit('setUser', response.user.uid);
    } catch (error) {
      this.$toast.error(error.message);
    }
  },
  async logout({ commit }) {
    try {
      await firebase.auth().signOut();

      commit('setUser', null);
      localStorage.removeItem('token');

      this.$toast.success('You are signed out of your account');
    } catch (error) {}
  },
};
