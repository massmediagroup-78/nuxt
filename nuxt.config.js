require('dotenv').config();

module.exports = {
  mode: 'spa',
  head: {
    title: 'nuxt',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Dancing+Script|Kulim+Park:400,700&display=swap',
      },
    ],
  },
  css: [
    { src: '~assets/styles/app.scss', lang: 'scss' },
    { src: 'swiper/dist/css/swiper.css', lang: 'css' },
    { src: 'aos/dist/aos.css', lang: 'css' },
  ],
  modules: ['bootstrap-vue/nuxt', '@nuxtjs/toast'],
  buildModules: [
    // Simple usage
    '@nuxtjs/dotenv',
  ],
  bootstrapVue: {
    components: [
      'BContainer',
      'BRow',
      'BCol',
      'BCard',
      'BFormInput',
      'BButton',
      'BNavbar',
      'BNavbarBrand',
      'BNavbarNav',
      'BNavbarToggle',
      'BCollapse',
      'BNavItem',
      'BNavItemDropdown',
      'BModal',
      'BImgLazy',
      'BImg',
      'BPaginationNav',
    ],
    directives: ['VBModal', 'VBTooltip'],
  },

  loading: { color: '#3B8070' },

  build: {
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',

          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
        config.module.rules.push({
          test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
          loader: 'file-loader',
        });
      }
    },
    transformAssetUrls: {
      video: ['src', 'poster'],
      source: 'src',
      img: 'src',
      image: 'xlink:href',
    },
  },
  plugins: ['~/plugins/firebase.js', '~/plugins/aos.js', { src: '~/plugins/vue-awesome-swiper', ssr: false }],
  toast: {
    position: 'top-right',
    duration: 3000,
  },
};
