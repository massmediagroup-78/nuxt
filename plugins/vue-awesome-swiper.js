import Vue from 'vue';
import VueAwesomeSwiper from 'vue-awesome-swiper';

Vue.use(VueAwesomeSwiper, {
  effect: 'fade',
  fadeEffect: {
    crossFade: true,
  },
  autoHeight: true,
  loop: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});
