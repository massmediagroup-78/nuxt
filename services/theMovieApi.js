import axios from 'axios';

const theMovieRoute = 'https://api.themoviedb.org/3';
const apiKey = 'e2e6c0d0272e7a10dfb1d89d1580078e';

const params = {
  api_key: apiKey,
};

export const getMoviesList = (page, genre) => {
  const params = {
    api_key: apiKey,
    page: page,
    with_genres: genre,
  };

  return axios.get(`${theMovieRoute}/discover/movie`, { params });
};
export const getSerialsList = (page, genre) => {
  const params = {
    api_key: apiKey,
    page: page,
    with_genres: genre,
  };

  return axios.get(`${theMovieRoute}/discover/tv`, { params });
};

export const getMovie = id => axios.get(`${theMovieRoute}/movie/${id}`, { params });
export const getNowPlayingMovie = () => axios.get(`${theMovieRoute}/movie/now_playing`, { params });
export const getLatestMovie = () => axios.get(`${theMovieRoute}/movie/latest`, { params });
export const getUpcomingMovie = () => axios.get(`${theMovieRoute}/movie/upcoming`, { params });
export const getPopularMovie = () => axios.get(`${theMovieRoute}/movie/popular`, { params });
export const getSerial = id => axios.get(`${theMovieRoute}/tv/${id}`, { params });
export const getOnTheAirSerial = () => axios.get(`${theMovieRoute}/tv/on_the_air`, { params });
export const getLatestSerial = () => axios.get(`${theMovieRoute}/tv/latest`, { params });
export const getPopularSerial = () => axios.get(`${theMovieRoute}/tv/popular`, { params });
export const getTopRatedMovie = page => {
  const params = {
    api_key: apiKey,
    page: page,
  };

  return axios.get(`${theMovieRoute}/movie/top_rated`, { params });
};
export const getTopRatedSerial = page => {
  const params = {
    api_key: apiKey,
    page: page,
  };

  return axios.get(`${theMovieRoute}/tv/top_rated`, { params });
};
export const getMovieReferences = id => axios.get(`${theMovieRoute}/movie/${id}/recommendations`, { params });
export const getSerialReferences = id => axios.get(`${theMovieRoute}/tv/${id}/recommendations`, { params });
export const getAllGenres = () => axios.get(`${theMovieRoute}/genre/movie/list`, { params });
export const getAllGenresTv = () => axios.get(`${theMovieRoute}/genre/tv/list`, { params });
export const getMovieCast = id => axios.get(`${theMovieRoute}/movie/${id}/credits`, { params });
export const getSerialCast = id => axios.get(`${theMovieRoute}/tv/${id}/credits`, { params });
export const getActor = id => axios.get(`${theMovieRoute}/person/${id}`, { params });
export const getActorMovies = id => axios.get(`${theMovieRoute}/person/${id}/movie_credits`, { params });
export const getSerialSeason = (id, number) => axios.get(`${theMovieRoute}/tv/${id}/season/${number}`, { params });
export const getSerialImages = id => axios.get(`${theMovieRoute}/tv/${id}/images`, { params });
export const getMovieImages = id => axios.get(`${theMovieRoute}/movie/${id}/images`, { params });
